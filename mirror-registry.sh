#!/bin/bash

export OCP_RELEASE='4.6.16'
export OCP_RELEASE_PATH='ocp'
export LOCAL_REPOSITORY='ocp4/openshift4'
export PRODUCT_REPO='openshift-release-dev'
export RELEASE_NAME='ocp-release'
export ARCHITECTURE='x86_64'
export REGISTRY_FQDN='ip-10-0-3-10......'
export GODEBUG='x509ignoreCN=0'


usage() {
    echo " ---- Script Descrtipion ---- "
    echo "  "
    echo " This script configures the bastion host that is meant to serve as local registry and core installation components of Red Hat Openshift 4"
    echo " "
    echo " Pre-requisites: "
    echo " "
    echo " Download the OCP installation secret in https://cloud.redhat.com/openshift/install/pull-secret and create a file called 'redhat-registry-pullsecret.json' in the $HOME directory"
    echo " "

    echo " "
    echo " Options:  "
    echo " "
    echo " * prep_dependencies: installs the os packages needed to perform the registry installation on RHEL8"
    echo " * get_artifacts: downloads and prepare the oc client and OCP installation program"
    echo " * prep_registry: create and configures the local registry"
    echo " * mirror_registry: mirrors the core registry container images for installation locally"
    echo " * prep_operators_catalogs: mirrors the core registry container images for installation locally"
    echo "  "
    echo -e " Usage: $0 [ prep_dependencies | get_artifacts | prep_registry | mirror_registry | prep_operators_catalogs ] "
    echo "  "
    echo " ---- Ends Descrtipion ---- "
    echo "  "
}


check_deps (){
    if [[ ! $(rpm -qa wget git bind-utils lvm2 lvm2-libs net-utils firewalld | wc -l) -ge 7 ]] ;
    then
        install_tools
    fi
}

get_artifacts() {
    cd ~/
    test -d artifacts || mkdir artifacts ; cd artifacts
    test -f openshift-client-linux-${OCP_RELEASE}.tar.gz  || curl -J -L -O https://mirror.openshift.com/pub/openshift-v4/clients/${OCP_RELEASE_PATH}/${OCP_RELEASE}/openshift-client-linux-${OCP_RELEASE}.tar.gz
    test -f openshift-install-linux-${OCP_RELEASE}.tar.gz || curl -J -L -O https://mirror.openshift.com/pub/openshift-v4/clients/${OCP_RELEASE_PATH}/${OCP_RELEASE}/openshift-install-linux-${OCP_RELEASE}.tar.gz
    cd ..
    prep_installer
}


install_tools() {
    #RHEL8
    if grep -q -i "release 8" /etc/redhat-release; then
        dnf -y install libguestfs-tools podman skopeo httpd haproxy bind bind-utils net-tools nfs-utils rpcbind wget tree git lvm2 lvm2-libs firewalld jq
        systemctl start firewalld
        echo -e "\e[1;32m Packages - Dependencies installed\e[0m"
    fi

    #RHEL7
    if grep -q -i "release 7" /etc/redhat-release; then
        #subscription-manager repos --enable rhel-7-server-extras-rpms
        yum -y install libguestfs-tools podman skopeo httpd haproxy bind-utils net-tools nfs-utils rpcbind wget tree git lvm2.x86_64 lvm2-libs firewalld bind bind-utils || echo "Please - Enable rhel7-server-extras-rpms repo" && echo -e "\e[1;32m Packages - Dependencies installed\e[0m"
        systemctl start firewalld
    fi
}

prep_registry () {
  test -d /registry | mkdir -p /registry/{auth,certs,data}
  openssl req -newkey rsa:4096 -nodes -sha256 -keyout /registry/certs/domain.key -x509 -days 365 -subj "/CN=${REGISTRY_FQDN}" -out /registry/certs/domain.crt
  cp -rf /registry/certs/domain.crt /etc/pki/ca-trust/source/anchors/
  update-ca-trust
  echo "Please enter admin user password"
  htpasswd -Bc /registry/auth/htpasswd admin
  podman run -d --name mirror-registry --net host -v /registry/data:/var/lib/registry:z -v /registry/auth:/auth:z -e "REGISTRY_AUTH=htpasswd" -e "REGISTRY_AUTH_HTPASSWD_REALM=registry-realm" -e "REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd" -v /registry/certs:/certs:z -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key quay.io/redhat-emea-ssa-team/registry:2
}

prep_installer () {
    cd ~/
    echo "Uncompressing installer and client binaries"
    test -d ~/bin/ || mkdir ~/bin/
    tar -xzf ./artifacts/openshift-client-linux-${OCP_RELEASE}.tar.gz  -C ~/bin
    tar -xaf ./artifacts/openshift-install-linux-${OCP_RELEASE}.tar.gz -C ~/bin
}

mirror_registry () {
  podman generate systemd --name mirror-registry > /etc/systemd/system/mirror-registry.service
  systemctl enable --now mirror-registry
  firewall-cmd --permanent --add-port=5000/tcp
  firewall-cmd --permanent --add-port=5000/udp
  firewall-cmd --reload
  podman login --authfile /root/mirror-registry-pullsecret.json "${REGISTRY_FQDN}:5000"
  jq -s '{"auths": ( .[0].auths + .[1].auths ) }' /root/mirror-registry-pullsecret.json /root/redhat-registry-pullsecret.json > /root/bundle-pullsecret.txt
  oc adm -a /root/bundle-pullsecret.txt release mirror --from=quay.io/${PRODUCT_REPO}/${RELEASE_NAME}:${OCP_RELEASE}-${ARCHITECTURE} --to=${REGISTRY_FQDN}:5000/${LOCAL_REPOSITORY} --to-release-image=${REGISTRY_FQDN}:5000/${LOCAL_REPOSITORY}:${OCP_RELEASE}-${ARCHITECTURE}
}


operators_catalogs () {
  echo "Create redhat-operators catalog image"
  echo " "
  oc adm catalog build --appregistry-org redhat-operators --from=registry.redhat.io/openshift4/ose-operator-registry:v4.5 --filter-by-os="linux/amd64" --registry-config=/root/bundle-pullsecret.txt --to=${REGISTRY_FQDN}:5000/olm/redhat-operators:v1
  echo " "

  echo "Mirror redhat-operators images "
  echo " "
  oc adm catalog mirror ${REGISTRY_FQDN}:5000/olm/redhat-operators:v1 ${REGISTRY_FQDN}:5000 --registry-config=/root/bundle-pullsecret.txt
  echo " "

  echo "Create certified-operators catalog image"
  echo " "
  oc adm catalog build --appregistry-org certified-operators --to=${REGISTRY_FQDN}:5000/olm/certified-operators:v1 --from=registry.redhat.io/openshift4/ose-operator-registry:v4.4 --registry-config=/root/bundle-pullsecret.txt
  echo " "

  echo "Mirror certified-operators images"
  echo " "
  oc adm catalog mirror ${REGISTRY_FQDN}:5000/olm/certified-operators:v1 ${REGISTRY_FQDN}:5000 --registry-config=/root/bundle-pullsecret.txt
  echo " "
}



key="$1"

case $key in
    get_artifacts)
        get_artifacts
        ;;
    mirror_registry)
        mirror_registry
        ;;
    prep_registry)
        prep_registry
        ;;
    prep_dependencies)
        install_tools
        ;;
    prep_operators_catalogs)
        operators_catalogs
        ;;
    *)
        usage
        ;;
esac
